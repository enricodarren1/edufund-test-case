import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { HomeComponent } from './components/home/home.component';
import { HowToBuyComponent } from './components/how-to-buy/how-to-buy.component';
import { ProcedureComponent } from './components/procedure/procedure.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent, pathMatch: 'full'},
  {path: 'procedure',component: ProcedureComponent,pathMatch: 'full'},
  { path: 'how-to-buy',component: HowToBuyComponent,pathMatch: 'full'},
  { path: 'about-us',component: AboutUsComponent,pathMatch: 'full'},
  {path: 'contact-us',component: ContactUsComponent,pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
