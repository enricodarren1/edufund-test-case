import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(
    private readonly http: HttpClient
  ) { }

  getBannerList() {
    return this.http.get('https://6319bfa08e51a64d2beb602d.mockapi.io/mock/v1/banners');
  }
}
