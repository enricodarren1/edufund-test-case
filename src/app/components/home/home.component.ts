import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  eduMenuList = [
    {
      title: 'EduCollege',
      icon: '../../assets/images/graduation-cap.png'
    },
    {
      title: 'EduSchool',
      icon: '../../assets/images/graduation-cap.png'
    },
    {
      title: 'EduCourse',
      icon: '../../assets/images/graduation-cap.png'
    },
    {
      title: 'EduBinar',
      icon: '../../assets/images/graduation-cap.png'
    },
    {
      title: 'EduCash',
      icon: '../../assets/images/graduation-cap.png'
    }
  ]

  eduDocuments = [
    {
      icon: './assets/images/person.png',
      title: 'Pekerja',
      documents1: ['KTP (Kartu Tanda Penduduk)', 'Bukti Penghasilan'],
      documents2: ['NPWP, atau', 'Slip Gaji, atau', 'Surat Keterangan Kerja, atau', 'Rekening Koran 1 bulan terakhir'],
      documents3: ['Tagihan pembayaran dari institusi pendidikan (selain EduCash)']
    },
    {
      icon: './assets/images/wiraswasta.png',
      title: 'Wiraswasta',
      documents1: ['KTP (Kartu Tanda Penduduk)', 'Bukti Penghasilan'],
      documents2: ['NPWP, atau', 'Rekening Koran 1 bulan terakhir'],
      documents3: ['Tagihan pembayaran dari institusi pendidikan (selain EduCash)']
    },
    {
      icon: './assets/images/pelajar.png',
      title: 'Pelajar',
      documents1: ['KTP (Kartu Tanda Penduduk)', 'Tagihan pembayaran dari institusi pendidikan (selain EduCash)']
    }
  ];

  bannerList: any = [];

  constructor(private readonly dataService: DataService) { }

  ngOnInit(): void {
    this.getBanner();
  }

  getBanner() {
    this.dataService.getBannerList().subscribe(res => {
      this.bannerList = res;
      console.log("banner list ", this.bannerList);
    })
  }

}
